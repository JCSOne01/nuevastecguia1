import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Guia 1 - Saludar',
      theme: ThemeData(primarySwatch: Colors.blue,),
      home: MyHomePage(title: 'Guia 1 - Saludar'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;
  
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final _helloController = TextEditingController();
  void _sayHi() { setState(() { _helloController.text = _helloController.text; }); }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(widget.title),),
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            TextField(
              controller: _helloController,
              decoration: InputDecoration(hintText: 'Nombre',),
            ),
            SizedBox(height: 20,),
            Text('Hola ' + _helloController.text, style: TextStyle(fontSize: 30),),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _sayHi,
        tooltip: 'Saludar',
        child: Icon(Icons.pan_tool),
      ),
    );
  }
}
